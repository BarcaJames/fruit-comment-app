import { Injectable } from '@angular/core';

import { Fruit } from './../entities/fruit';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';



@Injectable({
  providedIn: 'root'
})
export class FruitService {

  protected items: Fruit[] = [];

  constructor() {
    //  this.items.push(
    //    {
    //      id:1,
    //      name:'Apple',
    //      description:'Delicious Apple'
    //    } as Fruit
    //  )
    //  this.items.push(
    //   {
    //     id:1,
    //     name:'Banana',
    //     description:'Delicious Banana'
    //   } as Fruit
    // )
  }

  getAll(): Observable<Fruit[]> {

    // return of(this.items);
    let fruitsApi = ajax('http://localhost:9003/fruitapi/api/fruits.json')
      .pipe(
        map(res => {
          if (!res.response) {
            throw new Error("NO FRUIT DATA Received")
          }
          return res.response as Fruit[]
        }),
        catchError(err => {
          console.error(err);
          return of([] as Fruit[])
        })
      )
    return fruitsApi;
  }
}
