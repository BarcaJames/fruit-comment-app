import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable, of
} from 'rxjs';
import {Comment} from './../entities/comment';

@Injectable({
  providedIn: 'root'
})

export class CommentService {
  private comments: Comment[]=[];
  private lastAuthorName: BehaviorSubject<string>;

  constructor() { 
    this.lastAuthorName= new BehaviorSubject<string>('No Author as yet')
  }

  /**
   * @param comment Comment
   * @return Observable<boolean> success
   */
  public add(comment: Comment):Observable<boolean>{
    comment.id = this.comments.length+1; //generate id
    this.comments.push(comment); //Add comment to array
    //Add author name to subject
    this.lastAuthorName.next(comment.author);
    // Of function returns an observable of the value
    // passed to it. i.e. Observable value which is true
    return of(true)
  }

  getAll(): Observable<Comment[]>{
    return of(this.comments);
  }

  getLastAuthorName():Observable<String>{
    return this.lastAuthorName;
  }
}
