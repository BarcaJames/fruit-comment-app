import { Component, OnInit } from '@angular/core';
import {FruitService} from './../../services/fruit.service';
import {CommentService} from './../../services/comment.service';
import {Fruit} from './../../entities/fruit';
import {Comment} from './../../entities/comment';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-new-fruit-comment',
  templateUrl: './new-fruit-comment.component.html',
  styleUrls: ['./new-fruit-comment.component.scss']
})
export class NewFruitCommentComponent implements OnInit {

  fruits : Fruit[]=[];

  author:string="James";
  comment:string;
  fruitId:number;
  rating:number;

  constructor(
    private fruitService : FruitService,
    private commentService: CommentService
  ) { }

  ngOnInit() {
    
    this.fruitService
        .getAll()
        .subscribe(data =>{
          this.fruits = data;
        },error=>{
          console.error(error)
          window.alert("We were unable to retrieve your fruits at this time")
        });

  }

  addComment(){
    let comment:Comment = new Comment({
      author:this.author,
      comment:this.comment,
      fruitId:this.fruitId,
      rating: this.rating
    });
    let fruit = this.fruits.find((fruit)=>{
      return fruit.id == this.fruitId
    })
    comment.fruit = fruit;;
    //comment.setRating(this.rating)
    console.log("Adding comment by author",comment)
    this.commentService.add(comment)
         .pipe(
           catchError(err=>{
               console.error(err);
               window.alert("Unable to Post, please try again")
               return of(false);
           })
         )
         .subscribe(success=>{
              if(success){
                this.author = this.comment = "";
                window.alert("Posted successfully")
              }
              
         });
    
  }

}
