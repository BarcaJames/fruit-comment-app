import { Component, OnInit } from '@angular/core';
import {Comment} from './../../entities/comment';
import {CommentService} from './../../services/comment.service';

@Component({
  selector: 'app-component-listing',
  templateUrl: './component-listing.component.html',
  styleUrls: ['./component-listing.component.scss']
})

export class ComponentListingComponent implements OnInit {

  comments:Comment[];

  constructor(
    private commentService: CommentService
  ) { 
    this.commentService.add(
      {
        id:1,
        author: 'Bob',
        rating: 4,
        comment: 'I like pineapple'
      } as Comment
    );

    this.commentService.add(
      {
        id:2,
        author: 'Jamie',
        rating: 5,
        comment:' I like Guava'
      } as Comment
    );
  }

  ngOnInit() {
    this.commentService
        .getAll()
        .subscribe(allComments =>{
          this.comments = allComments
        })
  }

}
