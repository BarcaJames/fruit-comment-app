import { Comment } from './comment';

export class Fruit {
    id : number;
    name: string;
    description:string;
    comments?: Comment[]=[]; /*? means comment can be null aka nullabale */
}
